const Botkit = require("botkit");
const request = require("superagent");

if (!process.env.token) {
  console.log("Error: Specify a token in an environment variable");
  process.exit(1);
}

const controller = Botkit.slackbot();

controller
  .spawn({
    token: process.env.token
  })
  .startRTM();

controller.on("channel_joined", (bot, message) => {
  console.log(
    `${bot.identity.name} was invited to join the channel ${
      message.channel.name
    }`
  );
});

controller.hears(
  ["[0-9]+"],
  ["direct_message", "direct_mention", "mention", "ambient"],
  (bot, message) => {
    const number = message.match[0];
    request.get(`http://numbersapi.com/${number}`).end((err, res) => {
      if (!err) {
        bot.reply(message, res.text);
      } else {
        console.error(`Error: ${err}`);
      }
    });
  }
);

controller.on(
  ["direct_message", "direct_mention", "mention", "ambient", "message_channel"],
  (bot, message) => {
    bot.reply(message, message);
  }
);
